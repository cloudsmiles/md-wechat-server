const wechat = require('co-wechat')
const config = require('../config')

const cig = {
  token: config.token,
  appid: config.appid,
  encodingAESKey: config.encodingAESKey
}

module.exports = wechat(cig).middleware(async (message, ctx) => {
  if (message.Event === 'subscribe') {
    return {
      content: {
        title: "来段音乐吧",
        description: "狂欢吧"
      }
    }
  }
})