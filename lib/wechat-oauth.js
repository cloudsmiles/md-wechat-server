const OAuth = require('wechat-oauth')
const config = require('../config/')

const auth = new OAuth(config.appid, config.secret)
module.exports = auth