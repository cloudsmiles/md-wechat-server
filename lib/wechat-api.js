const WechatApi = require('wechat-api')
const config = require('../config/')

const api = new WechatApi(config.appid, config.appsecret)
module.exports = api