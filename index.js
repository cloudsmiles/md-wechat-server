const Koa = require('koa')
const Router = require('koa-router')
const server = require('./lib/wechat-server')

const app = new Koa()
const router = new Router()

router.get('/wechat', server)
app
  .use(router.routes())
  .use(router.allowedMethods())

app.listen(80)